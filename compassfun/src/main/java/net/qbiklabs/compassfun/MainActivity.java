package net.qbiklabs.compassfun;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.*;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.*;

import java.io.IOException;
import java.util.List;


public class MainActivity extends AppCompatActivity implements LocationListener, SensorEventListener {

    /**
     * Sensor variables
     */
    private LocationManager mLocationManager;

    private SensorManager mSensorManager;
    private Sensor mSensor;

    /**
     * View variables
     */
    private View dialogView;

    private AlertDialog destinationAlertDialog;

    private LinearLayout llDistance;
    private TextView tvDistanceValue;

    private ImageView ivCompassRose;
    private ImageView ivCompassNeedle;

    private FloatingActionButton fab;

    /**
     * Destination coordinates
     */
    private Double destinationLatitude;
    private Double destinationLongitude;

    /**
     * Multipiler for hemispheres
     */
    private double latitudeMultipiler = 1.0f;
    private double longitudeMultipiler = 1.0f;

    private Location currentLocation;

    /**
     * Animation degree variables
     */
    int degree = 0;
    int degreeNeedle = 0;

    int mAzimuth = 0;
    int mAzimuthNeedle = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Create sensor variables
         */
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        /**
         * Initiate veiw variables
         */
        llDistance = (LinearLayout) findViewById(R.id.llDistanceLayout);
        tvDistanceValue = (TextView) findViewById(R.id.tvDistanceValue);

        ivCompassRose = (ImageView) findViewById(R.id.compass_rose);
        ivCompassNeedle = (ImageView) findViewById(R.id.compass_needle);

        dialogView = getLayoutInflater().inflate(R.layout.dialog_coordinates, null);

        /**
         * AlertDialog for coordinates input
         */
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Your destination?");
        builder.setView(dialogView);

        /**
         * Dismiss dialog
         */
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        /**
         * Reset coordinates and disable location reading
         */
        builder.setNeutralButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                destinationLatitude = null;
                destinationLongitude = null;

                ivCompassNeedle.setVisibility(View.INVISIBLE);
                llDistance.setVisibility(View.INVISIBLE);

                unregisterLocationProvider();
            }
        });

        /**
         * Read data from input
         */
        builder.setPositiveButton("GO!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    destinationLatitude = Double.valueOf(((EditText) dialogView.findViewById(R.id.etLatitude)).getText().toString());
                    destinationLongitude = Double.valueOf(((EditText) dialogView.findViewById(R.id.etLongitude)).getText().toString());

                    /**
                     * Check if coordinates are valie
                     */
                    if ((destinationLatitude < 0.0 || destinationLatitude > 90.0)
                            || (destinationLongitude < 0.0 || destinationLongitude > 180.0)) {
                        final Snackbar snackBar = Snackbar.make(findViewById(R.id.fab), "Please provide valid coordinates.", Snackbar.LENGTH_INDEFINITE);
                        snackBar.setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackBar.dismiss();
                            }
                        });

                        snackBar.show();
                        return;
                    }
                } catch (NumberFormatException e) {
                    final Snackbar snackBar = Snackbar.make(findViewById(R.id.fab), "Wrong coordinates format.", Snackbar.LENGTH_INDEFINITE);
                    snackBar.setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackBar.dismiss();
                        }
                    });

                    snackBar.show();
                    return;
                }

                /**
                 * Apply directions
                 */
                destinationLatitude *= latitudeMultipiler;
                destinationLongitude *= longitudeMultipiler;

                /**
                 * Read location description
                 */
                if (Geocoder.isPresent()) {
                    try {
                        List<Address> addressList = new Geocoder(MainActivity.this).getFromLocation(destinationLatitude, destinationLongitude, 1);
                        if (addressList.size() > 0) {
                            Address address = addressList.get(0);

                            StringBuilder sb = new StringBuilder();
                            sb.append("Your destination: ");
                            if (address.getThoroughfare() != null) {
                                sb.append(address.getThoroughfare());
                                sb.append(", ");
                            }
                            if (address.getSubThoroughfare() != null) {
                                sb.append(address.getSubThoroughfare());
                                sb.append(", ");
                            }
                            if (address.getLocality() != null) {
                                sb.append(address.getLocality());
                                sb.append(", ");
                            }
                            if (address.getCountryName() != null) {
                                sb.append(address.getCountryName());
                            }
                            final Snackbar snackbarLocation = Snackbar.make(findViewById(R.id.fab), sb.toString(), Snackbar.LENGTH_INDEFINITE);
                            snackbarLocation.setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbarLocation.dismiss();
                                }
                            });
                            snackbarLocation.show();
                        }
                    } catch (IOException e) {
                        Log.d("GeoCoder", e.getMessage());
                    }
                }

                /**
                 * Show compass needle
                 */
                ivCompassNeedle.setVisibility(View.VISIBLE);

                /**
                 * Read GPS data for actual data and needle position
                 */
                registerLocationProvider();
            }
        });

        destinationAlertDialog = builder.create();

        /**
         * Set listener on Floating Action Button
         */
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destinationAlertDialog.show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * Reregister listeners
         */
        mSensorManager.registerListener(this, mSensor, 1000);

        if (destinationLatitude != null && destinationLongitude != null) {
            registerLocationProvider();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        /**
         * Unregister listeners
         */
        mSensorManager.unregisterListener(this, mSensor);

        unregisterLocationProvider();

    }

    /**
     * Regiter location listener
     */
    private void registerLocationProvider() {

        /**
         * Fine criteria for pbetter positioning
         */
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        String provider = mLocationManager.getBestProvider(criteria, true);

        /**
         * Check if provider is available
         */
        if (provider == null || mLocationManager.isProviderEnabled(provider)) {
            final Snackbar snackbarLocation = Snackbar.make(findViewById(R.id.fab), "Please turn on GPS.", Snackbar.LENGTH_INDEFINITE);
            snackbarLocation.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbarLocation.dismiss();
                }
            });
            snackbarLocation.show();

            return;
        }

        /**
         * Register updates
         */
        mLocationManager.requestLocationUpdates(provider, 2000, 0, this);
    }

    /**
     * Unregister location listener
     */
    private void unregisterLocationProvider() {
        mLocationManager.removeUpdates(this);
    }

    /**
     * Radio button checked reader
     */
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.rbLatituteNorth:
                if (checked)
                    latitudeMultipiler = 1.0;
                    break;
            case R.id.rbLatituteSouth:
                if (checked)
                    latitudeMultipiler = -1.0;
                    break;
            case R.id.rbLongitudeEast:
                if (checked)
                    longitudeMultipiler = 1.0;
                    break;
            case R.id.rbLongitudeWest:
                if (checked)
                    longitudeMultipiler = -1.0;
                    break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putParcelable("SAVE_LOCATION", currentLocation);

        outState.putDouble("LATITUDE", destinationLatitude);
        outState.putDouble("LONGITUDE", destinationLongitude);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        destinationLatitude = savedInstanceState.getDouble("LATITUDE");
        destinationLongitude = savedInstanceState.getDouble("LONGITUDE");

        Parcelable object = savedInstanceState.getParcelable("SAVE_LOCATION");
        if (object instanceof Location) {
            currentLocation = (Location) object;
        }
    }

    /**
     * Sensors listeners
     */

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {

            /**
             * Read compass rose orientation
             */
            float[] rMat = new float[9];
            float[] orientation = new float[3];

            SensorManager.getRotationMatrixFromVector( rMat, event.values );
            mAzimuth = (int) ( Math.toDegrees( SensorManager.getOrientation( rMat, orientation )[0] ) );

            RotateAnimation anim = new RotateAnimation(-degree, -mAzimuth,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                    0.5f);

            this.applyAnimationSetting(anim);

            ivCompassRose.startAnimation(anim);
            degree=mAzimuth;

            /**
             * Read compass rose orientation
             */
            if (currentLocation != null && destinationLatitude != null && destinationLongitude != null) {

                float[] result = new float[3];

                Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(), destinationLatitude, destinationLongitude, result);

                mAzimuthNeedle = (int) result[2];

                RotateAnimation needleAnimation = new RotateAnimation(degreeNeedle, mAzimuthNeedle - mAzimuth,
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                        0.5f);

                this.applyAnimationNeedleSetting(needleAnimation);

                ivCompassNeedle.startAnimation(needleAnimation);
                degreeNeedle = mAzimuthNeedle - mAzimuth;

                tvDistanceValue.setText(String.valueOf(result[0]));
                llDistance.setVisibility(View.VISIBLE);

            }

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * Location listeners
     */

    @Override
    public void onLocationChanged(Location location) {
        this.currentLocation = location;

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * Utils methods
     */

    private void applyAnimationSetting(Animation animation) {
        animation.setDuration(500);
        animation.setRepeatCount(0);
        animation.setFillAfter(true);
    }
    private void applyAnimationNeedleSetting(Animation animation) {
        animation.setDuration(1000);
        animation.setRepeatCount(0);
        animation.setFillAfter(true);
    }

}
